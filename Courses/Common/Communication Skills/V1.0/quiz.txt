(Type): multiplechoice
(Category): 6
(Question): What is one way you can keep people on your conference call focused and attentive?
(A): Read aloud the provided handouts during the call
(B): Use PowerPoint slides, so people can follow along
(C): Ask for frequent feedback and don't speak for too long at once
(D): Run your call as long as possible to make sure everyone understands
(Correct): C
(Points): 1
(WF): Speaking too long and not soliciting feedback bores the attendees; People have relatively short attention spans and just listening to a caller drone on will lose their attention.


(Type): multiplechoice
(Category): 6
(Question): What is the attention span of the average adult who attends a meeting?
(A): 30 Seconds
(B): 1 Minute
(C): 8 minutes
(D): 1 Hour
(E): 8 hours
(Correct): C
(Points): 1
(WF): It has been found that adults typically have an attention span of 8 minutes.


(Type): multiplechoice
(Category): 6
(Question): What is the most important component to being a successful consultant?
(A):  Trust
(B):  Sending emails
(C):  Being an expert
(D):  Being organized
(E):  Being charismatic
(Correct): A
(Points): 1
(WF): The training course's main theme is that Trust is the key component to being a successful consultant.


(Type): multiplechoice
(Category): 6
(Question): What is the most effective way to write an email?
(A):  Write the email in detail
(B):  Specify the CC and BCC all the time
(C):  Using acronyms to make the email short
(D):  Writing the main content in the opening sentence
(Correct): D
(Points): 1
(CF): The most effective way to write an email is to specify the main content in the opening sentence.


(Type): multiplechoice
(Category): 6
(Question): When scheduling a conference call, when should you send the agenda and materials?
(A):  During the call
(B):  Once, on the day of the call
(C):  Immediately following the call
(D):  Exactly three days prior to the call
(E):  Once prior to the call and one again on the day of the call
(Correct): E
(Points): 1
(CF): It's best to send the agenda and materials both prior to the call and on the day of the call, in case the call participants have lost them.


(Type): multiplechoice
(Category): 6
(Question): According to the Triune Brain Theory, what type of thinking is the "Lizard" portion of the brain responsible for?
(A):  Habits
(B):  Language
(C):  Emotions
(D):  Fight-or-flight response
(E):  Imagination and creativity
(Correct): D
(Points): 1
(CF): The "Lizard" portion of the brain handles the fight-or-flight response in humans.


(Type): multiplechoice
(Category): 6
(Question): If taking notes during an in-person meeting, what should you use to record them?
(A):  Tablet
(B):  Laptop
(C):  Mobile phone
(D):  Tape recorder
(E):  Pen and paper
(Correct): E
(Points): 1
(CF): Use pen and paper. It is less disruptive than an electronic device, and meeting participants will know you are paying attention.


(Type): multiplechoice
(Category): 6
(Question): When speaking during a meeting you are hosting; you should address...
(A): everyone, as it is appropriate
(B): your closest superior, because you report immediately to them
(C): the highest ranking person in the room, because they have the most influence
(D): the person you are most familiar with, because it will make you more comfortable
(Correct): A
(Points): 1
(WF): Since meetings should include only the people relevant to them, each person in the meeting is a valid candidate to be addressed, and likely will NEED to be addressed because they will have important input to help reach your meeting's objectives.


(Type): multiplechoice
(Category): 6
(Question): Why is making and distributing an agenda for your meeting important?
(A): It provides evidence to your superiors that you are taking the meeting seriously
(B): It provides a clear focus of intent and communicates what is expected to be covered
(C): It gives you an anchor to return to in case you lose your train of thought while speaking
(D): It gives you an excuse to reign in people that can't stay on the topics you want to discuss
(Correct): B
(Points): 1
(WF): Agendas provide a roadmap, so that everyone can know what to expect in the meetings they will attend. This will better help them prepare and contribute to the meeting.


(Type): multiplechoice
(Category): 6
(Question): Why is it important to follow up with attendees after you hold a meeting?
(A): It will keep the meeting topic on their minds
(B): It is important to have a large email presence, to get your name "out there"
(C): You can ensure that any tasks they need to perform are clearly explained and assigned and you can reinforce attendees' buy-in
(D): Most of them were probably off daydreaming or working on something else, so you want to try to maximize the potential for them to understand what your meeting was about
(Correct): C
(Points): 1
(WF): Following up with meeting attendees gives you and the other attendees a referenceable record of who is responsible for what task and helps to ensure that their buy-in is maintained


(Type): multiplechoice
(Category): 6
(Question): According to some surveys, what do almost all presentation attendees admit to doing during presentations?
(A): Sleep
(B): Daydream
(C): Work on something else
(D): Talk on their cell phones
(E): Pay their undivided attention
(Correct): B
(Points): 1
(WF): 91% admit to daydreaming. 39% admit to sleeping. According to the survey, at best, 9% might sit in rapt attention for the duration of the presentation.


(Type): multiplechoice
(Category): 6
(Question): Which statement is NOT a good guideline for presentation slides?
(A): 2 or 3 points can fit on a single slide if the points are small
(B): Try to order slides so that topics and points flow together smoothly
(C): Keep text to a minimum because people read quickly and have short attention spans
(D): Fonts and colors should be carefully considered and kept as simple as possible to improve readability
(Correct): A
(Points): 1
(WF): It is best to keep slides limited to 1 point per slide. Additional points on a slide can cause people to lose focus on the item you are discussing if they read ahead to the next points or their attention span runs out because there was too much text.


(Type): multiplechoice
(Category): 6
(Question): When dealing with adverse people __________
(A): it's ok to lose your temper if they are already being hostile
(B): listen to them carefully, repeat their concerns back, and answer them directly
(C): be friendly, keep your temper, but take the opportunity to punish them for causing problems
(D): answer directly, tell the truth, and try to bundle up multiple issues from the same person into a collective package to save time
(Correct): B
(Points): 1
(WF): It is important to treat even your adversaries with respect and try to resolve disputes professionally and peacefully. You want to keep them from interfering with your and, by extension, the company's success, and one of the best ways to do that is to get them on your side.


(Type): multiplechoice
(Category): 6
(Question): When someone who you believe to be required is missing from your meeting, how should you proceed?
(A):  Don't start the meeting until they arrive
(B):  Move through the presentation slowly to give them time to show up
(C):  Let the attendees know that you cannot begin without the individual
(D):  Move through the presentation as intended, attempt to bring the missing individual up to speed later
(Correct): D
(Points): 1
(WF): You should not waste the other attendees time for being punctual.


(Type): multiplechoice
(Category): 6
(Question): What are three effective ways to keep your audience engaged and help them retain information? (Choose three)
(A): Ask them questions
(B): Link slides together
(C): Use analogies and stories
(D): Maintain a calm even tone that does not vary.
(E): Stop the presentation for jokes if you see them dozing off
(Correct): A,B,C
(Points): 1
(WF): Humor is not always an appropriate way to engage someone.  You need to stay focused on your goal.


(Type): multiplechoice
(Category): 6
(Question): As a consultant, what are two important things to convey to an executive? (Choose two)
(A): That you are a trusted advisor
(B): That you are not replacing them
(C): That you are technically superior
(D): That you will look out for their bottom line
(Correct): A,D
(Points): 1
(WF): They know that you are not here to replace them, and you were hired for your knowledge, so you need not convince them that you are technically superior to them. They care about the bottom line and need to trust your advice.


(Type): multiplechoice
(Category): 6
(Question): When is it appropriate to inject sarcasm into a business email?
(A):  Never
(B):  Whenever it seems appropriate
(C):  All the time. People love a good joke
(D):  You should use sarcasm sparingly and tactfully
(E):  If there is an applicable Dilbert cartoon you can attach
(Correct): A
(Points): 1
(WF): Sarcasm is a subjective thing and can easily be misinterpreted. Avoid injecting sarcasm in all business emails.


(Type): multiplechoice
(Category): 6
(Question): What is the most important part about sending email communications?
(A):  Including every detail
(B):  Being succinct and to the point
(C):  Making sure you CC everything to your manager
(D):  Neglecting to include your phone number or contact information
(E):  Including your displeasure in your email, so you can convey yourself
(Correct): B
(Points): 1
(WF): Being succinct and to the point helps with conveying the facts, ensures your message was understood, and respects the recipient's time.


(Type): multiplechoice
(Category): 6
(Question): When someone comes into your workspace with an issue while you are on a conference call, what is the most appropriate response?
(A):  Mute the call and work with the person quickly
(B):  Place the call on hold and help that person as long as needed
(C):  Hang up so other attendees are aware that you had something pressing come up
(D):  Place the call on hold and quickly ask the person to come back after the meeting
(Correct): A
(Points): 1
(WF): Never use hold, it will often flood the conference call with hold music.  Hanging up is generally poor etiquette.


(Type): multiplechoice
(Category): 6
(Question): When creating an email, which of the following statements is true?
(A):  You should follow up on an unanswered email no earlier than 24 hours later
(B):  You should include as little extra as possible, only covering the important facts
(C):  You should reread an email that was written when angry carefully for possible errors
(D): Take your time writing a response to a serious message to be sure you have covered all bases
(Correct): B
(Points): 1
(WF): important emails should be answered quickly, you should not resend checking on unanswered emails, and you should never write an angry email.


(Type): multiplechoice
(Category): 6
(Question): The easiest fonts to read for most people are?
(A):  Serif Fonts
(B):  Script Fonts
(C):  Italic Fonts
(D):  San-Serif Fonts
(Correct): D
(Points): 1
(WF): San-serif fonts include Arial or Helvetica.  Other types are harder to read.


(Type): multiplechoice
(Category): 6
(Question): How many meetings do people consider to be a waste of time?
(A):  1/4
(B):  1/2
(C):  3/4
(D):  All
(Correct): B
(Points): 1
(WF): The average person considers half of their meetings to be a waste of time.


(Type): multiplechoice
(Category): 6
(Question): A key stakeholder has brought up a topic unrelated to the agenda for the meeting, how should you handle this situation?
(A):  Utilize the "Parking Lot" technique
(B):  Respond that this is off topic and move on
(C):  Entertain the idea until the topic is resolved
(D):  Ask the stakeholder to email the comment to you after the meeting has ended
(Correct): A
(Points): 1
(WF): By adding a topic to the parking lot, the new (unrelated) subject will not be forgotten, as it is documented for discussion in the future.


(Type): multiplechoice
(Category): 6
(Question): What is good advice for someone attending a meeting?
(A):  Do not place your hands on the table
(B):  Try to prove your technical superiority
(C):  Point your finger when addressing someone
(D):  Never interrupt someone, even if you disagree
(E):  Make an attempt to sell more services to your client if possible
(Correct): D
(Points): 1
(WF): You should never interrupt someone who is speaking, even if you disagree with what they are saying.
