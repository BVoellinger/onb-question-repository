(STARTIGNORE)

Note: Any text within the (STARTIGNORE) and (ENDIGNORE) tags will be ignored when importing this question template

INSTRUCTIONS
Fill in the blank question templates below then upload and check them under the "Tests -> Question bank -> Import questions" section.
Maximum of 50 questions can be uploaded at once

Your categories numbers are listed below for your reference to add to the (Category): line
For example, the line below will use the 'Generic' category

(Category):

Your categories:
0 Generic
1 Java
2 Axure
3 JIRA
4 OOP
5 SDLC
6 Communication
7 Unix Scripting
8 HTML
9 CSS
10 JavaScript
11 Angular JS
12 Design Patterns
13 Git
14 Java Design Pattern
15 Business Analysis
16 Version Control
17 Presentations
18 SQL
19 Database Design

(Grade style): Only applicable for 'multiplechoice' (with more then 1 correct answer) style questions
0: Give full score only when all correct responses are provided
1: Give partial score per correct response and deduct partial score for incorrect responses
2: Give partial score per correct response but do not deduct partial score for incorrect responses

(Submitter): This is the name of the person creating and submitting the questions.

(Course Site): This is the site the course was taken on. The options for this are Lynda, and Udemy.

(Course Name): This is the exact name of the course as it is written on the site.

(Course URL): The URL of the course.

(Discipline): This is what discipline the training is classified under, Professional or Technical.

(Hint): This is an optional field that can be used if you wish to provide a hint to the user about the question.

(Subject): This is similar to category but can be used to specify more specific detail on the category.

(Difficulty): This is a range of difficulty of the question, Beginner, Intermediate, and Advanced.

(Applicability): This is if the question is about the course or can be applied to the category in general. Course or General are accepted choices for this section.


(ENDIGNORE)

(STARTIGNORE)
Quiz Details
(Submitter):
(Course Site):
(Course Name):
(Course URL):
(Discipline):
(ENDIGNORE)

(Type): multiplechoice
(Category): 1
(Grade style): 0
(Random answers): 0
(Question): Assuming all necessary methods are defined, which of the following Java class declarations will compile?
(A): public class TestClass extends Object implements Comparable, Serializable {}
(B): public class TestClass extends Object, Comparable implements Serializable {}
(C): public class TestClass extends Object, Comparable, Serializable {}
(D): public class TestClass implements Object {}
(E): None of the above.
(Correct): A
(Points): 1
(CF): Java only allows a class to extend a single parent class; it can implement as many interfaces as required. 
(WF): Java only allows a class to extend a single parent class; it can implement as many interfaces as required. 
(STARTIGNORE)
(Hint): 
(Subject): Core Concepts
(Difficulty): Beginner
(ENDIGNORE)

(Type): multiplechoice
(Category): 1
(Grade style): 0
(Random answers): 0
(Question): Reflection allows for which of the following?
(A): The ability to call multiple methods in a chain.
(B): The ability to inspect and manipulate an object's methods and fields at runtime.
(C): The ability to roll back the results of a method call.
(D): The ability to make an object appear to be a different type of object.
(E): None of the above.
(Correct): B
(Points): 1
(CF): (B) - Reflection provides the capability to obtain information about an object's structure and act upon it without knowing initial details.
(WF): (B) - Reflection provides the capability to obtain information about an object's structure and act upon it without knowing initial details.
(STARTIGNORE)
(Hint): 
(Subject): Reflection
(Difficulty): Intermediate
(ENDIGNORE)

(Type): multiplechoice
(Category): 1
(Grade style): 0
(Random answers): 0
(Question): Which of the following is a primitive data type?
(A): Boolean
(B): Short
(C): Long
(D): Double
(E): None of the above.
(Correct): E
(Points): 1
(CF): (E) - Capitalization matters - 'boolean' (a primitive) is not the same as its wrapper class, 'Boolean' (an object).
(WF): (E) - Capitalization matters - 'boolean' (a primitive) is not the same as its wrapper class, 'Boolean' (an object).
(STARTIGNORE)
(Hint): 
(Subject): Data Types
(Difficulty): Beginner
(ENDIGNORE)

(Type): multiplechoice
(Category): 1
(Grade style): 0
(Random answers): 1
(Question): Which of the following represents the definition of the program entry point?
(A): private int main (arc, argv);
(B): protected void main ();
(C): public static int main (string args);
(D): public static void main (String[] args);
(E): static int main();
(Correct): D
(Points): 1
(CF): The main entry point for a Java program consists of 'public' (can be called from anywhere in the program), 'static' (does not need a class instance to be invoked), 'void' (does not return a value), 'main' (the method name) and finally, a String array passed in for program arguments (String[] args).
(WF): The main entry point for a Java program consists of 'public' (can be called from anywhere in the program), 'static' (does not need a class instance to be invoked), 'void' (does not return a value), 'main' (the method name) and finally, a String array passed in for program arguments (String[] args).
(STARTIGNORE)
(Hint):
(Subject): Class Structure
(Difficulty): Beginner
(ENDIGNORE)

(Type): multiplechoice
(Category): 1
(Grade style): 0
(Random answers): 1
(Question): The following method is defined within interface 'TestInterface':

public void testMethod(String testStr);

Which of the following methods, if added to 'TestInterface', will NOT cause a compilation error?

(A): public void testMethod(int testInt);
(B): public void testMethod(String testStrTwo);
(C): private void testMethod(String testStr);
(D): public int testMethod(String testStr);
(E): private int testMethod(String testStrTwo);
(Correct): A
(Points): 1
(CF): (A) - A method can be overloaded if its parameter list is different. All other options share a method signature with the original method.
(WF): (A) - A method can be overloaded if its parameter list is different. All other options share a method signature with the original method.
(STARTIGNORE)
(Hint):
(Subject): Class Structure
(Difficulty): Intermediate
(ENDIGNORE)

(Type): multiplechoice
(Category): 1
(Grade style): 0
(Random answers): 1
(Question): Which collection is best suited to managing unsorted, unique data requiring frequent lookups? 
(A): ArrayList
(B): HashSet
(C): LinkedList
(D): TreeSet
(E): HashMap
(Correct): B
(Points): 1
(CF): (B) - HashSets are unsorted, cannot contain duplicate values and have superior (O(1)) lookup performance.
(WF): (B) - HashSets are unsorted, cannot contain duplicate values and have superior (O(1)) lookup performance.
(STARTIGNORE)
(Hint):
(Subject): Collections
(Difficulty): Intermediate
(ENDIGNORE)

(Type): multiplechoice
(Category): 1
(Grade style): 0
(Random answers): 1
(Question): Which collection is best suited to managing sorted data that may contain duplicates? 
(A): ArrayList
(B): HashSet
(C): LinkedList
(D): TreeSet
(E): HashMap
(Correct): A
(Points): 1
(CF): (A) - ArrayLists allow for data sorting and can contain duplicate values.
(WF): (A) - ArrayLists allow for data sorting and can contain duplicate values.
(STARTIGNORE)
(Hint):
(Subject): Collections
(Difficulty): Intermediate
(ENDIGNORE)

(Type): multiplechoice
(Category): 1
(Grade style): 0
(Random answers): 1
(Question): Which collection is best suited to supporting key-value pairs? 
(A): ArrayList
(B): HashSet
(C): LinkedList
(D): TreeSet
(E): HashMap
(Correct): E
(Points): 1
(CF): (E) - HashMap is the only collection in this list that supports key-value pairs.
(WF): (E) - HashMap is the only collection in this list that supports key-value pairs.
(STARTIGNORE)
(Hint):
(Subject): Collections
(Difficulty): Intermediate
(ENDIGNORE)

(Type): multiplechoice
(Category): 1
(Grade style): 0
(Random answers): 1
(Question): Consider the following code:

public class TestClass {
	{ // code to print "A" to the console }
	
	static{	// code to print "B" to the console	}

	public TestClass(){ // code to print "C" to the console }
	
	public static void main(String [] args){ 
		// code to print "D" to the console
		new TestClass(); 
	}	
}

What will be output upon executing this program?

(A): A
     B
	 C
	 D
(B): D
     C
(C): B
     D
	 A
	 C
(D): D
     B
	 A
	 C
(E): A compilation error occurs.
(Correct): C
(Points): 1
(CF): (C) - The code is executed in the following order - static initializer, main method, initializer, constructor.
(WF): (C) - The code is executed in the following order - static initializer, main method, initializer, constructor.
(STARTIGNORE)
(Hint):
(Subject): Class Structure
(Difficulty): Advanced
(ENDIGNORE)

(Type): multiplechoice
(Category): 1
(Grade style): 0
(Random answers): 0
(Question): Consider the following code segment:

Object testStr = new String();
if(testStr instanceof String){
	// prints "testStr is an instance of String" to the console
}else{ 
	// prints "testStr is NOT an instance of String" to the console
}

Assuming no issues with any code not displayed, what occurs when this code is executed?

(A): A compilation error occurs.
(B): A runtime error occurs.
(C): "testStr is an instance of String" is printed to the console.
(D): "testStr is NOT an instance of String" is printed to the console.
(E): None of the above.
(Correct): C
(Points): 1
(CF): (C) - testStr instanceof String will evaluate to true, causing the related print to execute.
(WF): (C) - testStr instanceof String will evaluate to true, causing the related print to execute.
(STARTIGNORE)
(Hint): 
(Subject): Core Concepts
(Difficulty): Beginner
(ENDIGNORE)


(Type): multiplechoice
(Category): 1
(Grade style): 0
(Random answers): 0
(Question): Which of the following most closely defines the purpose of the hashCode() method? 
(A): It is used to create a String representation of an object.
(B): It is used to compare two objects.
(C): It is used to generate a random value.
(D): It is used to create an integer representation of an object.
(E): It is used to compress an object for serialization.
(Correct): D
(Points): 1
(CF): (D) - hashCode() returns the result of a hash function executed on the object as an integer.
(WF): (D) - hashCode() returns the result of a hash function executed on the object as an integer.
(STARTIGNORE)
(Hint): 
(Subject): Core Methods
(Difficulty): Beginner
(ENDIGNORE)

(Type): multiplechoice
(Category): 1
(Grade style): 0
(Random answers): 0
(Question): Which of the following statements regarding Object is true?
(A): Object is an abstract class.
(B): All variables (e.g. int, String) are subclasses of Object.
(C): If a class extends Object, it must implement the equals() method.
(D): Object cannot be subclassed with the extends keyword.
(E): None of the above.
(Correct): E
(Points): 1
(CF): None of the above. Object is a concrete class (it can be instantiated). Only non-primitive types are subclasses of Object. It is not required that equals() be implemented to extend Object - it has its own definition in Object. Object can be subclassed just like any other class in Java, using the extends keyword.
(WF): None of the above. Object is a concrete class (it can be instantiated). Only non-primitive types are subclasses of Object. It is not required that equals() be implemented to extend Object - it has its own definition in Object. Object can be subclassed just like any other class in Java, using the extends keyword.
(STARTIGNORE)
(Hint): 
(Subject): Class Structure
(Difficulty): Beginner
(ENDIGNORE)

(Type): multiplechoice
(Category): 1
(Grade style): 0
(Random answers): 0
(Question): The class 'MyAbstractClass' is an abstract class. It is extended and implemented by the 'MyFirstSubclass' and 'MySecondSubclass' classes. Which of the following statements will compile?

1 - MyAbstractClass m1 = new MyFirstSubclass();
2 - MyAbstractClass m2 = new MySecondSubclass();
3 - MyAbstractClass m3 = new MyAbstractClass();
4 - MySecondSubclass m4 = new MySecondSubclass();
5 - MySecondSubclass m5 = new MyAbstractClass();

(A): 3 only
(B): 1 and 2
(C): 3 and 4
(D): 3 and 5
(E): 1, 2 and 4
(Correct): E
(Points): 1
(CF): (E) - 'MyAbstractClass' cannot be instantiated. All other statements shown are valid in this case.
(WF): (E) - 'MyAbstractClass' cannot be instantiated. All other statements shown are valid in this case.
(STARTIGNORE)
(Hint): 
(Subject): Class Structure
(Difficulty): Intermediate
(ENDIGNORE)

(Type): multiplechoice
(Category): 1
(Grade style): 0
(Random answers): 0
(Question): The class 'MySubclass' extends 'MyAbstractClass', which is an abstract class containing only the abstract method myAbstractMethod(). Which of the following MUST be true of 'MySubclass'? 
(A): It MUST implement myAbstractMethod().
(B): It MUST be declared abstract.
(C): Either A or B. 
(D): Both A and B.
(E): None of the above.
(Correct): C
(Points): 1
(CF): (C) - A subclass of an abstract class must either implement any abstract methods in its parent OR be declared as abstract itself. While it is possible to do both, it is not required.
(WF): (C) - A subclass of an abstract class must either implement any abstract methods in its parent OR be declared as abstract itself. While it is possible to do both, it is not required.
(STARTIGNORE)
(Hint): 
(Subject): Class Structure
(Difficulty): Beginner
(ENDIGNORE)

(Type): multiplechoice
(Category): 1
(Grade style): 0
(Random answers): 0
(Question): Which sequence correctly orders the Java access modifiers from least restrictive to most restrictive? 

(A): public, private, protected, default
(B): private, default, protected, public
(C): public, protected, default, private
(D): public, private, protected, default
(E): public, default, protected, private
(Correct): C
(Points): 1
(CF): (C) - The correct order is public, protected, default, private. Remember that protected provides default access plus access for any subclass.
(WF): (C) - The correct order is public, protected, default, private. Remember that protected provides default access plus access for any subclass.
(STARTIGNORE)
(Hint): 
(Subject): Access Modifiers
(Difficulty): Beginner
(ENDIGNORE)

(Type): multiplechoice
(Category): 1
(Grade style): 0
(Random answers): 0
(Question): The static variable 'testStatic' exists in class 'TestStaticClass'. If 10 instances of 'TestStaticClass' are created, how many instances of 'testStatic' exist?

(A): 10
(B): 1
(C): 0
(D): It depends on how the 10 instances are created.
(E): None of the above. 
(Correct): B
(Points): 1
(CF): (B) - Only one will be created; static variables belong to the class, not to instances of it.
(WF): (B) - Only one will be created; static variables belong to the class, not to instances of it.
(STARTIGNORE)
(Hint): 
(Subject): static
(Difficulty): Beginner
(ENDIGNORE)

(Type): multiplechoice
(Category): 1
(Grade style): 0
(Random answers): 0
(Question): Consider the following code:

public class TestStaticClass {

	private static int testStatic = 0;
	
	public TestStaticClass(){
		testStatic++;
		
		// prints the value of testStatic to the console
		printStatic();
	}
}

What will be output if 3 instances of 'TestStaticClass' are created?

(A): 0 
     0 
	 0
(B): 1
     1 
	 1
(C): 0 
     1 
	 2
(D): 1
     2
	 3
(E): None of the above. 
(Correct): D
(Points): 1
(CF): (D) - The 'testStatic' variable will be shared across all 3 instances.
(WF): (D) - The 'testStatic' variable will be shared across all 3 instances.
(STARTIGNORE)
(Hint): 
(Subject): static
(Difficulty): Intermediate
(ENDIGNORE)

(Type): multiplechoice
(Category): 1
(Grade style): 0
(Random answers): 0
(Question): The class 'BaseClass' resides in package 'com.test.one' and declares a variable as follows:

int testValue;

The class 'OtherPackageSubclass' extends 'BaseClass' and resides in package 'com.test.two'. Which of the following statements is true?

(A): 'OtherPackageSubclass' will have access to testValue because it extends 'BaseClass'.
(B): 'OtherPackageSubclass' will not have access to testValue because it belongs to a different package than 'BaseClass'. 
(C): A compilation error will occur because 'testValue' has no access modifier (e.g. public, private).
(D): A compilation error will occur because you cannot extend a class that belongs to a different package.
(E): None of the above.
(Correct): B
(Points): 1
(CF): (B) - Even though 'OtherPackageSubclass' extends 'BaseClass', the default access modifier does not give visibility to subclasses that exist outside of the parent's package. Only the 'protected' and 'public' modifiers would allow this access.
(WF): (B) - Even though 'OtherPackageSubclass' extends 'BaseClass', the default access modifier does not give visibility to subclasses that exist outside of the parent's package. Only the 'protected' and 'public' modifiers would allow this access.
(STARTIGNORE)
(Hint): 
(Subject): Access Modifiers
(Difficulty): Intermediate
(ENDIGNORE)

(Type): multiplechoice
(Category): 1
(Grade style): 0
(Random answers): 0
(Question): Which of the following statements summarize the equals() - hashCode() contract? 

1 - A syntax error will be thrown if equals and hashCode are not implemented in every class.
2 - If a.equals(b) is true, then a.hashCode() should be equal to b.hashCode().
3 - a.equals(b) must return the same value as a.hashCode(b). 
4 - If equals() is implemented for a class, hashCode() should also be implemented for that class.

(A): 1 only
(B): 3 only
(C): 2 and 4 
(D): 3 and 4
(E): 1, 2 and 4
(Correct): C
(Points): 1
(CF): (C) - The equals() - hashCode() contract is not a syntactic requirement, but dictates best practices for their implementation. 2, 4 and 5 describe these practices.
(WF): (C) - The equals() - hashCode() contract is not a syntactic requirement, but dictates best practices for their implementation. 2, 4 and 5 describe these practices.
(STARTIGNORE)
(Hint): 
(Subject): Core Methods
(Difficulty): Intermediate
(ENDIGNORE)

(Type): multiplechoice
(Category): 1
(Grade style): 0
(Random answers): 0
(Question): Which of the following is true of an interface? 
(A): It is identical to a class.
(B): It usually contains method implementations.
(C): It cannot be extended.
(D): It cannot be instantiated. 
(E): None of the above.
(Correct): D
(Points): 1
(CF): (D) - An interface cannot be instantiated - it has no constructor nor implementation.
(WF): (D) - An interface cannot be instantiated - it has no constructor nor implementation.
(STARTIGNORE)
(Hint): 
(Subject): Class Structure
(Difficulty): Beginner
(ENDIGNORE)

(Type): multiplechoice
(Category): 1
(Grade style): 0
(Random answers): 0
(Question): Which of the following statements are true of the compareTo() method?
(A): It is defined in the Object class and returns an integer identifying which of two objects is logically greater than the other.
(B): It is defined in the Comparable interface and returns an integer identifying which of two objects is logically greater than the other.
(C): It is defined in the Object class and returns true if the calling object is greater than the parameter object, and false otherwise.
(D): It is defined in the Comparable interface and returns true if the calling object is greater than the parameter object, and false otherwise.
(E): None of the above.
(Correct): B
(Points): 1
(CF): (B) - The compareTo() method is declared in the Comparable interface. In the example a.compareTo(b), 1 is returned if a is logically greater than b, -1 if the opposite is true, and 0 if they are equivalent.
(WF): (B) - The compareTo() method is declared in the Comparable interface. In the example a.compareTo(b), 1 is returned if a is logically greater than b, -1 if the opposite is true, and 0 if they are equivalent.
(STARTIGNORE)
(Hint): 
(Subject): Core Methods
(Difficulty): Beginner
(ENDIGNORE)

(Type): multiplechoice
(Category): 1
(Grade style): 0
(Random answers): 0
(Question): In a try-catch-finally block, what best describes the behavior of the finally block?
(A): It will execute unless an exception occurs.
(B): It will execute unless an exception DOES NOT occur.
(C): It will always execute.
(D): It will execute unless the program crashes or is exited (e.g. via System.exit(0)) prior to the finally block.
(E): It will execute unless the function returns prior to the finally block.
(Correct): D
(Points): 1
(CF): (D) - Only an event that causes the program to exit will prevent a finally block from executing.
(WF): (D) - Only an event that causes the program to exit will prevent a finally block from executing.
(STARTIGNORE)
(Hint): 
(Subject): Error-Handling
(Difficulty): Intermediate
(ENDIGNORE)

(Type): multiplechoice
(Category): 1
(Grade style): 0
(Random answers): 0
(Question): What is true about a call to System.gc()?
(A): A request is made to the garbage collector, but there is no guarantee that it will run.
(B): It will cause a compile error.
(C): Only the main method can call it.
(D): It will return the amount of free memory.
(E): None of the above.
(Correct): A
(Points): 1
(CF): (A) - System.gc() makes a request to the garbage collector but there is no guarantee it will run.
(WF): (A) - System.gc() makes a request to the garbage collector but there is no guarantee it will run.
(STARTIGNORE)
(Hint):
(Subject): Memory Management
(Difficulty): Advanced
(ENDIGNORE)

(Type): multiplechoice
(Category): 1
(Grade style): 0
(Random answers): 0
(Question): Which of the following is a limitation of basic arrays?
(A): They can contain only primitive types.
(B): They cannot be declared statically.
(C): They cannot be instantiated with literal values.
(D): They require more memory than an ArrayList of the same size.
(E): They cannot be resized at runtime.
(Correct): E
(Points): 1
(CF): (E) - The only valid limitation from this list is that basic arrays cannot be resized at runtime.
(WF): (E) - The only valid limitation from this list is that basic arrays cannot be resized at runtime.
(STARTIGNORE)
(Hint):
(Subject): Collections
(Difficulty): Intermediate
(ENDIGNORE)

(Type): multiplechoice
(Category): 1
(Grade style): 0
(Random answers): 0
(Question): Consider the following code:

public class TestClass {
	public static void testUpdate(int testInt, String testStr){
		testInt = 5;
		testStr = "10";
	}
	
	private static void printValues(){
		// code to print the values of testInt and testStr to the console
	}
		
	public static void main(String [] args){
		int testInt = 1;
		String testStr = "2";
		
		printValues();
		TestClass.testUpdate(testInt, testStr);
		printValues();		
	}	
}

Assuming no issues with any code not displayed, what occurs when this code is executed?

(A): A compilation error occurs.
(B): A runtime error occurs.
(C): "1 2 | 5 10" is printed to the console.
(D): "1 2 | 1 2" is printed to the console.
(E): "1 2 | 1 10" is printed to the console.
(Correct): D
(Points): 1
(CF): (D) - A new String object is created when 'testStr' is assigned to "10" within the testUpdate method.
(WF): (D) - A new String object is created when 'testStr' is assigned to "10" within the testUpdate method.
(STARTIGNORE)
(Hint):
(Subject): Class Structure
(Difficulty): Advanced
(ENDIGNORE)

(Type): multiplechoice
(Category): 1
(Grade style): 0
(Random answers): 0
(Question): Consider the following code:

private static String testTryCatchFinally(){
	try{
		throw new Exception();
	}catch(Exception e){
		return "catch";
	}finally{
		return "finally";
	}
}

What will be returned when the 'testTryCatchFinally' method is called?

(A): The String "catch".
(B): The String "finally".
(C): A null value.
(D): Nothing; the method will exit when the exception is thrown and control will return to the calling function.
(E): None of the above; a compilation error will occur.
(Correct): B
(Points): 1
(CF): (B) - The return statement in the finally block will take precedence over that in the catch block. This method will always return "finally" unless program execution is interrupted.
(WF): (B) - The return statement in the finally block will take precedence over that in the catch block. This method will always return "finally" unless program execution is interrupted.
(STARTIGNORE)
(Hint): 
(Subject): Error-Handling
(Difficulty): Advanced
(ENDIGNORE)

(Type): multiplechoice
(Category): 1
(Grade style): 0
(Random answers): 0
(Question): The use of an Iterator helps to facilitate which of the following?
(A): Addition of elements into a Collection.
(B): Dynamic resizing of primitive arrays.
(C): Removal of elements from a Collection during traversal.
(D): Sorting of elements in a Collection.
(E): Quickly accessing a specific element in a Collection.
(Correct): C
(Points): 1
(CF): (C) - Removing elements from a Collection during traversal will incur a ConcurrentModificationException without the help of an Iterator.
(WF): (C) - Removing elements from a Collection during traversal will incur a ConcurrentModificationException without the help of an Iterator.
(STARTIGNORE)
(Hint):
(Subject): Collections	
(Difficulty): Advanced
(ENDIGNORE)

(Type): multiplechoice
(Category): 1
(Grade style): 0
(Random answers): 0
(Question): The use of generics help to facilitate which of the following?
(A): Increased simplicity of subclassing Object.
(B): The ability to extend multiple classes.
(C): The ability to use the Collections framework with primitive datatypes.
(D): Automatic typecasting between primitive datatypes and their Object-based counterparts (e.g. int and Integer)
(E): The ability to utilize a single class to handle multiple different types of objects without type casting.
(Correct): E
(Points): 1
(CF): (E) - Generics can be used to increase code flexibility and accept and return multiple object types without type casting. The Collections framework is a great example of this feature.
(WF): (E) - Generics can be used to increase code flexibility and accept and return multiple object types without type casting. The Collections framework is a great example of this feature.(STARTIGNORE)
(Hint):
(Subject): Generics	
(Difficulty): Intermediate
(ENDIGNORE)

(Type): multiplechoice
(Category): 1
(Grade style): 0
(Random answers): 0
(Question): Consider the following class definition:

public class TestGenerics<T extends ClassA & InterfaceB> {

}

Which of the following is true?

(A): Only 'Class A' or 'InterfaceB' can be supplied as type parameters to the class 'TestGenerics'.
(B): Type parameters to the class 'TestGenerics' must extend 'ClassA' and implement 'InterfaceB'.
(C): Type parameters to the class 'TestGenerics' can only be provided if 'ClassA & InterfaceB' evaluates to true.
(D): The code will not compile, since interfaces are implemented, not extended.
(E): The code will not compile because type 'T' has not been defined.
(Correct): B
(Points): 1
(CF): (B) - 'T extends ClassA & InterfaceB' simply means that any type parameters to the class 'TestGenerics' must extend 'ClassA' and implement 'InterfaceB'. In this case, the keyword "extends" is used for both interfaces and parent classes.
(WF): (B) - 'T extends ClassA & InterfaceB' simply means that any type parameters to the class 'TestGenerics' must extend 'ClassA' and implement 'InterfaceB'. In this case, the keyword "extends" is used for both interfaces and parent classes.(STARTIGNORE)
(Hint):
(Subject): Generics	
(Difficulty): Advanced
(ENDIGNORE)

(Type): multiplechoice
(Category): 1
(Grade style): 0
(Random answers): 0 l, orld
(Question): Given the String s = "Hello, World!", which of the following will evaluate to "HELLO W"?

(A): s.substring(8).replace(',', '\0').toUpperCase();
(B): s.substring(0,5) + s.substring(6,1)).toUpperCase();
(C): s.substring(0,5).toUpperCase();
(D): s.substring(0,5) + s.substring(6,8)).toUpperCase();
(E): s.replace(',', '\0').substring(0,8).toUpperCase();
(Correct): D
(Points): 1
(CF): (D) - The correct choice extracts "Hello" from the String, extracts "W" from the String, and then converts both pieces to capital letters.
(WF): (D) - The correct choice extracts "Hello" from the String, extracts "W" from the String, and then converts both pieces to capital letters.
(Hint):
(Subject): Strings	
(Difficulty): Advanced
(ENDIGNORE)
